#!/usr/bin/env python

import os
import rospy
import numpy as np
import cv2
import random as rd
from drone_wrapper import DroneWrapper
from std_msgs.msg import Bool, Float64
from sensor_msgs.msg import Image
from geometry_msgs.msg import Twist

PDP_HUNTER_DIR = os.getenv('PDP_HUNTER_DIR')
AREA_SIZE = 10
rd.seed = 1
new_vec = [rd.randrange(-10, 10, 1)/ 100, rd.randrange(-10, 10, 1)/ 100, 0, rd.randrange(-10, 10, 1)/ 100]
# new_vec = [-0.5, 0, 0, 0]
code_live_flag = False

subjects = ["", "Giorno Giovanna", "Not Giorno", "Astray"]
face_recognizer_path = os.path.join(PDP_HUNTER_DIR, 'yooo.xml')
face_cascase_path = os.path.join(PDP_HUNTER_DIR, 'haarcascade_frontalface_default.xml')
#face_recognizer = cv2.face.LBPHFaceRecognizer_create()
face_recognizer = cv2.face.createLBPHFaceRecognizer()
face_recognizer.load(face_recognizer_path)
face_cascade = cv2.CascadeClassifier(face_cascase_path)

def gui_takeoff_cb(msg):
    if msg.data:
        drone.takeoff()
    else:
        drone.land()

def gui_play_stop_cb(msg):
    global code_live_flag, code_live_timer
    if msg.data == True:
        if not code_live_flag:
            code_live_flag = True
            code_live_timer = rospy.Timer(rospy.Duration(nsecs=50000000), execute)
    else:
        if code_live_flag:
            code_live_flag = False
            code_live_timer.shutdown()

def gui_twist_cb(msg):
    global drone
    drone.set_cmd_vel(msg.linear.x, msg.linear.y, msg.linear.z, msg.angular.z)

def set_image_filtered(img):
    gui_filtered_img_pub.publish(drone.bridge.cv2_to_imgmsg(img))

def set_image_threshed(img):
    gui_threshed_img_pub.publish(drone.bridge.cv2_to_imgmsg(img))

def execute(event):
    global drone
    global new_vec
    global last_x, last_y, last_yaw
    img_frontal = drone.get_frontal_image()
    img_ventral = drone.get_ventral_image()
    # Both the above images are cv2 images
    ################# Insert your code here #################################
    face, rect = detect_face(img_ventral)
    
    drone_position = drone.get_position()
    drone_x = drone_position[0]
    drone_y = drone_position[1]
    drone_z = drone_position[2]
    # z = 0

    print(img_ventral.shape)
    ####### buat height ##############
    if (drone_z >= 1):
        new_vec[2] = -0.2   
    elif (drone_z <= 0.5):
        new_vec[2] = 0.2
    #################################   
    if face is not None and rect is not None:
        #label = face_recognizer.predict(face)  # error bcz incompatible opencv2
        label = (1, 24.34)
        ## Buat lokasi inti #######
        x, y, w, h = rect
        facex, facey = (x + h/2, y + w/2)
        resx, resy = facex - img_ventral.shape[1]/2, facey - img_ventral.shape[0]/2
        if (resx == 0 or resy == 0):
            sign = [0, 0]
        else:
            sign = [resx/ abs(resx), resy / abs(resy)]
        new_vec[0] = 0.12*sign[0]
        new_vec[1] = 0.12*sign[1]
        # print('Drone Position = ', drone_position)
        # print('CHANGED')
        # print(facex, facey)
        ##########################
        label_text = subjects[label[0]]
        draw_rectangle(img_ventral, rect)
        draw_text(img_ventral, label_text, rect[0], rect[1]-5)
    ############ Ngatur radius ############################
    if (drone_x > AREA_SIZE and drone_y > AREA_SIZE):
        new_vec[0] = -1
        new_vec[1] = -1
    elif (drone_x > AREA_SIZE and drone_y < -AREA_SIZE):
        new_vec[0] = -1
        new_vec[1] = 1
    elif (drone_x < -AREA_SIZE and drone_y > AREA_SIZE):
        new_vec[0] = 1
        new_vec[1] = -1        
    elif (drone_x < -AREA_SIZE and drone_y < -AREA_SIZE):
        new_vec[0] = 1
        new_vec[1] = 1    
    ######################################################
    print(new_vec)
    drone.set_cmd_vel(*new_vec)   # Buat gerak
    set_image_filtered(img_frontal)
    set_image_threshed(img_ventral)
    #########################################################################

def detect_face(img):
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    equ = cv2.equalizeHist(gray)

    #let's detect multiscale images(some images may be closer to camera than others)
    #result is a list of faces
    faces = face_cascade.detectMultiScale(equ, 1.1, minNeighbors=3)

    #if no faces are detected then return original img
    if (len(faces) == 0):
        return None, None

    #under the assumption that there will be only one face,
    #extract the face area
    x, y, w, h = faces[0]

    #return only the face part of the image
    return gray[y:y+w, x:x+h], faces[0]

def detect_face2(img):
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    equ = cv2.equalizeHist(gray)


    #load OpenCV face detector, I am using LBP which is fast
    #there is also a more accurate but slow: Haar classifier
    face_cascade = cv2.CascadeClassifier('./drive/My Drive/Dataset/haarcascade_frontalface_default.xml')

    #let's detect multiscale images(some images may be closer to camera than others)
    #result is a list of faces
    faces = face_cascade.detectMultiScale(equ, 1.1, minNeighbors=3);

    #if no faces are detected then return original img
    if (len(faces) == 0):
        return None, None

    #if more than 1 face
    if (len(faces) > 1):
        region = []
        for face in faces:
            x, y, w, h = face
            region.append(gray[y:y+w, x:x+h])
        return region, faces

    #under the assumption that there will be only one face,
    #extract the face area
    x, y, w, h = faces[0]
    #return only the face part of the image
    return gray[y:y+w, x:x+h], faces[0]

def predict2(test_img):
    #make a copy of the image as we don't want to change original image
    img = test_img.copy()
    #detect face from the image
    #print(img)
    faces, rect = detect_face2(img)
    #print(face)
    if len(faces) > 1:
        for i in range(len(faces)):
            #predict the image using our face recognizer
            label = face_recognizer.predict(faces[i])
            print(label)
            #get name of respective label returned by face recognizer
            label_text = subjects[label[0]]
            #draw a rectangle around face detected
            draw_rectangle(img, rect[i])
            #draw name of predicted person
            draw_text(img, label_text, rect[i][0], rect[i][1]-5)
    else:
        label = face_recognizer.predict(faces[0])
        print(label)
        #get name of respective label returned by face recognizer
        label_text = subjects[label[0]]
        #draw a rectangle around face detected
        draw_rectangle(img, rect)
        #draw name of predicted person
        draw_text(img, label_text, rect[0], rect[1]-5)

    return img

def draw_rectangle(img, rect):
    x, y, w, h = rect
    cv2.rectangle(img, (x, y), (x+w, y+h), (0, 255, 0), 2)

def draw_text(img, text, x, y):
    '''
    function to draw text on give image starting from
    passed (x, y) coordinates.
    '''
    cv2.putText(img, text, (x, y), cv2.FONT_HERSHEY_PLAIN, 1.5, (0, 255, 0), 2)

if __name__ == "__main__":
    drone = DroneWrapper()
    rospy.Subscriber('gui/takeoff_land', Bool, gui_takeoff_cb)
    rospy.Subscriber('gui/play_stop', Bool, gui_play_stop_cb)
    rospy.Subscriber('gui/twist', Twist, gui_twist_cb)
    gui_filtered_img_pub = rospy.Publisher('interface/filtered_img', Image, queue_size = 1)
    gui_threshed_img_pub = rospy.Publisher('interface/threshed_img', Image, queue_size = 1)
    code_live_flag = False
    code_live_timer = rospy.Timer(rospy.Duration(nsecs=50000000), execute)
    code_live_timer.shutdown()

    while not rospy.is_shutdown():
        rospy.spin()
